--Truy vấn hai thành phố trong STATION với tên THÀNH PHỐ ngắn nhất và dài nhất cũng như độ dài tương ứng của chúng (ví dụ: số ký tự trong tên). Nếu có nhiều thành phố nhỏ nhất hoặc lớn nhất, hãy chọn thành phố đứng đầu khi sắp xếp theo thứ tự bảng chữ cái.
SELECT
    station.city,
    MIN(LENGTH (city)) AS do_dai
FROM
    station
ORDER BY
    city
    -- Truy vấn danh sách tên THÀNH PHỐ bắt đầu bằng nguyên âm (tức là, a, e, i, o hoặc u) từ STATION, không phân biệt chữ HOA chữ thường . Kết quả của bạn không chứa bản trùng.
SELECT DISTINCT
    CITY
FROM
    STATION
WHERE
    LOWER(SUBSTR (CITY, 1, 1)) IN ('a', 'e', 'i', 'o', 'u');

--Truy vấn Tên của bất kỳ học sinh nào trong HỌC SINH đạt điểm cao hơn 75 . Sắp xếp đầu ra của bạn theo ba ký tự cuối cùng của mỗi tên. Nếu hai hoặc nhiều học sinh đều có tên kết thúc bằng ba ký tự cuối giống nhau (ví dụ: Bobby, Robby, v.v.), hãy sắp xếp chúng theo ID tăng dần .
SELECT
    student_name
FROM
    STUDENTS
WHERE
    GRADE > 75
ORDER BY
    RIGHT (student_name, 3),
    ID;